import React from "react";
import { Route, Switch, useLocation, Redirect } from "react-router-dom";
// import { useTranslation } from "react-i18next";
import "./index.css";
// import context from "../../context";
import { observer } from "mobx-react";
import "antd/dist/antd.css";
import Home from "../Home";
import Dashbord from "../Dashbord";
import Schedule from "../Schedule";
import Gantt from "../Gantt";
import Login from "../Login";
import Register from "../Register";
import { UserLayout } from "../../component/Layout";
// import routes from "../../common/routes";

const App = () => {
  // const { global } = useContext(context);
  let location = useLocation();
  // UserLayout的LeftMenu选中的标记设置
  let select = "1";
  switch (location.pathname) {
    case "/":
      select = "1";
      break;
    case "/home":
      select = "2";
      break;
    case "/schedule":
      select = "3";
      break;
    case "/gantt":
      select = "4";
      break;
    default:
      select = "1";
  }
  // 登录页面和注册页面的跳转
  // 1、跳转的url是登录页面和注册页面的url
  // 2、当前是非登录状态，即token失效或者token不存在，即auth校验不通过
  // 两者同时满足的场合下，才能跳转到登录和注册页面
  const isLoginURL = location.pathname === "/login" || location.pathname === "/register";

  // 内部访问页面是否可以访问需要进行auth判断
  // 如果当前用户auth判断不通过则跳转到登录页面

  // 综上跳转到登录页面
  // 非登录状态，当前模拟场合只规定token是否有值，有：OK/无：NG
  let token = "sss";
  const isAuth = token !== undefined && token !== null && token !== "";
  // 登录状态，访问login或者register页面的时候，重定向到首页
  const isRedirect = isLoginURL && isAuth;

  // const { i18n } = useTranslation();
  // useEffect(() => {
  //   i18n.changeLanguage(global.language);
  // }, [global.language, i18n]);

  return (
    <div className="App">
      {!isAuth ? (
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
        </Switch>
      ) : isRedirect ? (
        <UserLayout select={["1"]}>
          <Redirect
            to={{
              pathname: "/"
            }}
          />
        </UserLayout>
      ) : (
        <UserLayout select={[select]}>
          <Switch>
            <Route path="/" exact>
              <Dashbord />
            </Route>
            <Route path="/home">
              <Home />
            </Route>
            <Route path="/schedule">
              <Schedule />
            </Route>
            <Route path="/gantt">
              <Gantt />
            </Route>
          </Switch>
        </UserLayout>
      )}

      {/* <About />
      <UpdateModal /> */}
    </div>
  );
};

export default observer(App);
