import { Breadcrumb } from "antd";

const Dashbord = () => {
  return (
    <div>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Dashbord</Breadcrumb.Item>
      </Breadcrumb>
      <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
        Dashbord Page!
      </div>
    </div>
  )
}

export default Dashbord;
