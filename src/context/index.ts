import { createContext } from "react";
import store from "../store";

const context = createContext(store);

export default context;
