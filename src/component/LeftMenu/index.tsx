import { Layout, Menu } from "antd";
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import React from "react";
import "./index.css";

const { Sider } = Layout;
// const { SubMenu } = Menu;

type LeftMenuType = {
  select: string[];
  collapsed: boolean;
}

const CommonLeftMenu: React.FC<LeftMenuType> = ({ select, collapsed }) => {
  const history = useHistory();
  const handleClick = (e: any) => {
    switch (e.key) {
      case "1":
        history.push("/");
        break;
      case "2":
        history.push("/home");
        break;
      case "3":
        history.push("/schedule");
        break;
      case "4":
        history.push("/gantt");
        break;
      default:
        history.push("/home");
    }
  };

  // const collapse = () => {
  //   collapsed = !collapsed;
  // }

  return (
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={select} mode="inline" onClick={(e) => handleClick(e)}>
        <Menu.Item key="1" icon={<PieChartOutlined />}>
          Option 1
        </Menu.Item>
        <Menu.Item key="2" icon={<DesktopOutlined />}>
          Option 2
        </Menu.Item>
        <Menu.Item key="3" icon={<FileOutlined />}>
          Schedule
        </Menu.Item>
        <Menu.Item key="4" icon={<TeamOutlined />}>
          Gantt
        </Menu.Item>
        {/* <SubMenu key="sub1" icon={<UserOutlined />} title="User">
          <Menu.Item key="3">Tom</Menu.Item>
          <Menu.Item key="4">Bill</Menu.Item>
          <Menu.Item key="5">Alex</Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
          <Menu.Item key="6">Team 1</Menu.Item>
          <Menu.Item key="8">Team 2</Menu.Item>
        </SubMenu> */}
        {/* <Menu.Item key="9" icon={<FileOutlined />}>
          Files
        </Menu.Item> */}
      </Menu>
    </Sider>
  );
};

export default CommonLeftMenu;
