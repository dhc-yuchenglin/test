import { Layout } from 'antd';
import "./BasicLayout.css";

const { Header, Footer, Content } = Layout;

const BasicLayout = (props: any) => {
  const { children } = props;
  return (
    <Layout style={{ minHeight: "100vh" }} className="basic-layout">
      <Header style={{ background: "#7dbcea", textAlign: "center", color: "#ffffff"}}>Schedule System Login</Header>
      <Content className="basic-layout-content">{children}</Content>
      <Footer style={{ background: "#7dbcea", textAlign: "center", color: "#ffffff" }} className="basic-layout-footer basic-layout-footer-background">DTC ©2021 Created by Front End Team</Footer>;
    </Layout>
  )
}

export default BasicLayout;
