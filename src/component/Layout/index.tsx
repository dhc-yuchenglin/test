import UserLayout from "./UserLayout";
import BasicLayout from "./BasicLayout";

export {
  BasicLayout,
  UserLayout
}
