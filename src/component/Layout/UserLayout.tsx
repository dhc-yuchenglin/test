import { Layout } from "antd";
import CommonFooter from "../Footer";
import CommonLeftMenu from "../LeftMenu";
import "./UserLayout.css";
import React, { useState } from "react";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';

const { Header, Content } = Layout;

type BasicLayoutProps = {
  children: any;
  select: string[];
};

const UserLayout: React.FC<BasicLayoutProps> = (props) => {
  const { children, select } = props;
  const [ collapsed, onToggle ] = useState(false);
  const toggle = () => onToggle(collapsed => !collapsed);

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <CommonLeftMenu select={select} collapsed={collapsed}></CommonLeftMenu>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: "trigger",
            onClick: () => {toggle()},
          })}
        </Header>
        <Content style={{ margin: '0 16px' }}>
          {children}
        </Content>
        <CommonFooter />
      </Layout>
    </Layout>
  );
};

export default UserLayout;
