import { Layout } from "antd";
import "./index.css";

const { Header } = Layout;

type BasicHeaderProps = {
  children: any;
};
const CommonHeader:React.FC<BasicHeaderProps> = (props) => {
  const { children } = props;
  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      {children}
    </Header>
  )
}

export default CommonHeader;
