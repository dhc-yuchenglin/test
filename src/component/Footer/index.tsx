import { Layout } from "antd";

const { Footer } = Layout;

const CommonFooter = () => {
  return <Footer style={{ textAlign: "center" }}>DTC ©2021 Created by Front End Team</Footer>;
}

export default CommonFooter;
